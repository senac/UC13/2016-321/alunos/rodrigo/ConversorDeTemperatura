/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.test;

import br.com.rodrigo.conversordetemperatura.ConversorTemperatura;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Diamond
 */
public class ConversorTest {
    
   
    @Test
    public void testandoConversaoParaCelsiusParaFahrenheitTest() {
        double fahrenheit = 212;
        double resultado = ConversorTemperatura.ConverterParaCelsius(fahrenheit);

        Assert.assertEquals(100, resultado, 0);
        
        if (resultado == 100) {
            System.out.println("Deu certo....");
        } else {
            System.out.println("Deu errado...");
        }
         
    }

    @Test
    public void testandoConversaoParaFahrenheitParaCelsiusTest() {

        double celsius = 100;
        double resultado = ConversorTemperatura.ConverterParaFahrenheit(celsius);

        Assert.assertEquals(212, resultado, 0);
    }

    
}
