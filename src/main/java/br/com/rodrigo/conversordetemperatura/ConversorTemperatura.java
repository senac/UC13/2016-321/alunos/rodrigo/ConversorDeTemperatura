/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.conversordetemperatura;

/**
 *
 * @author Diamond
 */
public class ConversorTemperatura {
    
     public static double ConverterParaFahrenheit(double celsius){
        return ((celsius * 9.0)/5.0) + 32 ; 
    }
    
    public static double ConverterParaCelsius(double fahrenheit ){
        return (5.0 * (fahrenheit - 32 ))/9 ; 
    }
    
}
